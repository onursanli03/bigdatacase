# Welcome

Go ahead and try:

```
$ git clone https://onursanli03@bitbucket.org/onursanli03/bigdatacase.git/wiki
```

First create needed containers using docker-compose.yml

...
$ docker-compose up -d
...

after finishing setups these containers should be created
![Capture.PNG](https://bitbucket.org/repo/BkejGax/images/2181197845-Capture.PNG)

kafka image uses port 9092, zookeeper 2181 and mongo 27000

Spring-Boot rest api generates logs and post the kafka producer. After that kafka consumer takes the logs push to the mongodb.

Before the running spring-boot app please run

...
$ mvn clean install
...

### running after the app using the postman or any other api test tools post to the localhost:8080/api/kafka