import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import ChartComponent from './Components/ChartComponent'
import DBservices from './Services/DBservices.js'
import moment from 'moment'

class App extends Component {

  render() {
    return (
      <div className="App">
        <ChartComponent chartData={this.state.chartData} location="Massachusetts" legendPosition="bottom"/>
      </div>
    );
  }
}

export default App;
