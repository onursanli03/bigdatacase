import React,{Component} from 'react'
import '../Services/DBservices.js'
import axios from 'axios'
import { Line} from 'react-chartjs-2';


class ChartComponent extends Component{

    constructor(props){
        super(props);
        this.state = {
            Data:{}
        }
    }

    componentDidMount(){
        axios.get('http://localhost:8080/api/kafka').then(
            res=>{
                const log = res.Data;
                let cities = [];
                let citiesLogs = [];

                log.array.forEach(element => {
                    cities.push(element.logLevelServerCityName);
                    citiesLogs.push(element.logLevelServerCityName.count());
                });
                this.setState({
                    Data: {
                        datasets:[
                            {
                                label:cities,
                                fill:false,
                                data:citiesLogs,
                            }
                        ]
                    }
                })
            }
        )
    }

 
    render(){
        return(
            <div className="container">
                <h1>Line Chart</h1>
                <Line data={this.state.Data} options = {{ maintainAspectRatio: false }} />
            </div>
        )
    }

}
export default ChartComponent