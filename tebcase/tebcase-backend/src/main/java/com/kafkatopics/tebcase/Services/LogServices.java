package com.kafkatopics.tebcase.Services;

import com.kafkatopics.tebcase.Models.Log;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class LogServices {

    private static List<Log> logs = new ArrayList<>();
    private static LocalDateTime dateTime = LocalDateTime.now();

    static {
        logs.add(new Log(dateTime,"INFO","Istanbul","Hello-from-Istanbul"));
        logs.add(new Log(dateTime,"WARN","Tokyo","Hello-from-Tokyo"));
        logs.add(new Log(dateTime,"FATAL","Moskow","Hello-from-Moskow"));
        logs.add(new Log(dateTime,"DEBUG","Beijing","Hello-from-Beijing"));
        logs.add(new Log(dateTime,"ERROR","London","Hello-from-London"));
    }

    public List<Log> getLogs() {
        return logs;
    }

}
