package com.kafkatopics.tebcase.Controller;

import com.google.gson.Gson;
import com.kafkatopics.tebcase.DAO.LogRepository;
import com.kafkatopics.tebcase.Models.Log;
import com.kafkatopics.tebcase.Services.LogServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/kafka")
public class KafkaController {

    private KafkaTemplate<String,String> kafkaTemplate;
    private Gson jsonConverter;

    @Autowired
    private LogRepository repository;

    @Autowired
    public KafkaController(KafkaTemplate<String, String> kafkaTemplate, Gson jsonConverter){

        this.kafkaTemplate = kafkaTemplate;
        this.jsonConverter = jsonConverter;

    }

    @PostMapping
    public void send() throws InterruptedException {

        LogServices services = new LogServices();
        List<Log> logs = services.getLogs();
        while(true) {
            for (Log log : logs) {
                Thread.sleep(1000);
                kafkaTemplate.send("my-replicated-topic", jsonConverter.toJson(log));
            }
        }
    }

    @PostMapping("/v2")
    public void post(@RequestBody Log log) throws InterruptedException {
       kafkaTemplate.send("my-replicated-topic", jsonConverter.toJson(log));
    }



    @KafkaListener(topics = "my-replicated-topic")
    public void getFromKafka(String log){

        System.out.println(log);

        Log log1 = (Log) jsonConverter.fromJson(log,Log.class);

        System.out.println(log1.toString());

        repository.save(log1);

    }

    @GetMapping
    public List<Log> getLogs(){
        return repository.findAll();
    }

    @GetMapping("/count/{city}")
    public List<Log> getCountLogs(@PathVariable String city){

        Log log = new Log();

        return repository.findAllBy(city);
    }



}
