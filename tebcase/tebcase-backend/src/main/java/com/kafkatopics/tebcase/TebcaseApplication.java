package com.kafkatopics.tebcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TebcaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(TebcaseApplication.class, args);
    }

}
