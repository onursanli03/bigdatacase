package com.kafkatopics.tebcase.DAO;

import com.kafkatopics.tebcase.Models.Log;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<Log,String> {

     List<Log> findAll();
     List<Log> findAllBy(String criteria);

}
