package com.kafkatopics.tebcase.Models;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;


@Getter
@Setter
public class Log implements Serializable {


    public static final String COLLECTION_NAME="Logs";

    private LocalDateTime timestamp;
    private String logLevel;
    private String logLevelServerCityName;
    private String logDetail;

    public Log() {}

    public Log(LocalDateTime timestamp, String logLevel, String logLevelServerCityName, String logDetail) {

        this.timestamp = timestamp;
        this.logLevel = logLevel;
        this.logLevelServerCityName = logLevelServerCityName;
        this.logDetail = logDetail;
    }

    @Override
    public String toString() {
        return "Log{" +
                ", timestamp=" + timestamp +
                ", logLevel=" + logLevel +
                ", logLevelServerCityName=" + logLevelServerCityName  +
                ", logDetail=" + logDetail  +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Log)) return false;
        Log log = (Log) o;
        return getTimestamp().equals(log.getTimestamp()) &&
                getLogLevel().equals(log.getLogLevel()) &&
                getLogLevelServerCityName().equals(log.getLogLevelServerCityName()) &&
                getLogDetail().equals(log.getLogDetail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp(), getLogLevel(), getLogLevelServerCityName(), getLogDetail());
    }
}
